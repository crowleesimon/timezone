package com.crowleysimon.timezonethingy.app;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by simon on 29/04/2014.
 */
public class ListViewAdapter extends BaseAdapter {

    Activity context;
    String cities[];

    public ListViewAdapter(Activity context, String cities[]) {
        super();
        this.context = context;
        this.cities = cities;
    }

    public int getCount() {
        return cities.length;
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    private class ViewHolder {
        TextView txtViewTitle;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = context.getLayoutInflater();

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.timezone_item, null);
            holder = new ViewHolder();
            holder.txtViewTitle = (TextView)convertView.findViewById(R.id.list_item);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.txtViewTitle.setText(cities[position]);

        return convertView;
    }

}
