package com.crowleysimon.timezonethingy.app;

import android.app.Activity;
import android.os.Bundle;
import android.text.format.Time;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.util.Log;

import java.util.TimeZone;

import com.crowleysimon.timezonethingy.app.Cities;


public class MainActivity extends Activity {

    final String POS = "UTC +";
    final String NEG = "UTC ";
    final int NUM_ZONES = 27;

    Cities city = new Cities();


    //UI elements
    SeekArc seekbar;
    TextView zoneText;
    TextView timeText;
    RelativeLayout RL;
    ImageView img;
    ListView cityList;

    int timezone;
    Time now;
    int hour = 0;
    int minute = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //UI elements
        RL = (RelativeLayout)findViewById(R.id.layout_id);
        seekbar = (SeekArc)findViewById(R.id.timezone_slider);
        zoneText = (TextView)findViewById(R.id.timezone_text);
        timeText = (TextView)findViewById(R.id.time_text);
        img = (ImageView)findViewById(R.id.img);
        cityList = (ListView)findViewById(R.id.city_listview);



        timezone = TimeZone.getDefault().getRawOffset() / 3600000; //returns the difference in hrs from UTC
        changeList(timezone);
        now = new Time();
        now.setToNow();
        hour = Integer.parseInt(now.format("%H"));
        minute = Integer.parseInt(now.format("%M"));
        timeText.setText(hour + ":" + minute);

        RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_0));

        if(hour >= 5 && hour <= 17) { //daytime (the image is set to a moon by default)
            img.setImageResource(R.drawable.sun);
        }

        if(timezone >= 0 && timezone <= 14) {
            zoneText.setText(POS + timezone + "");
        }else {
            zoneText.setText(NEG + (timezone - NUM_ZONES) + "");
        }

        seekbar.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {

            int tempTZ;
            int tempHR;
            @Override
            public void onProgressChanged(SeekArc seekArc, int i, boolean b) {
                tempTZ = timezone + i;
                tempHR = hour + i;

                if(tempTZ > 12) {
                    tempTZ -= 25;
                }

                if(tempTZ >= 0 && tempTZ <= 12) {
                    zoneText.setText(POS + tempTZ + "");
                }else {
                    zoneText.setText(NEG + tempTZ + "");
                }

                if(tempHR > 23) {
                    tempHR -= 24;
                }

                if(tempHR >= 5 && tempHR <= 17) {
                    img.setImageResource(R.drawable.sun);
                }else {
                    img.setImageResource(R.drawable.moon);
                }

                timeText.setText(String.valueOf(tempHR) + ":" + minute);

                changeBackground(tempHR);
                changeList(tempTZ);
            }

            @Override
            public void onStartTrackingTouch(SeekArc seekArc) {

            }

            @Override
            public void onStopTrackingTouch(SeekArc seekArc) {

            }
        });
    }

    public void changeBackground(int hour) {
        switch(hour){
            case 0:
            case 1: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_0));
                    break;
            case 2: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_1));
                    break;
            case 3:
            case 4: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_2));
                    break;
            case 5: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_3));
                    break;
            case 6: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_4));
                    break;
            case 7: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_5));
                    break;
            case 8: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_6));
                    break;
            case 9:
            case 10: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_7));
                     break;
            case 11:
            case 12: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_8));
                     break;
            case 13:
            case 14: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_7));
                     break;
            case 15:
            case 16: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_6));
                     break;
            case 17: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_5));
                     break;
            case 18: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_4));
                     break;
            case 19: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_3));
                     break;
            case 20: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_2));
                     break;
            case 21:
            case 22: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_1));
                     break;
            case 23: RL.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_0));
                     break;
        }
    }

    public void changeList(int tz) {
        switch(tz) {
            case -12:  cityList.setAdapter(new ListViewAdapter(this, city.neg_12));
                        break;
            case -11:  cityList.setAdapter(new ListViewAdapter(this, city.neg_11));
                        break;
            case -10:  cityList.setAdapter(new ListViewAdapter(this, city.neg_10));
                        break;
            case -9:   cityList.setAdapter(new ListViewAdapter(this, city.neg_9));
                        break;
            case -8:   cityList.setAdapter(new ListViewAdapter(this, city.neg_8));
                        break;
            case -7:   cityList.setAdapter(new ListViewAdapter(this, city.neg_7));
                        break;
            case -6:   cityList.setAdapter(new ListViewAdapter(this, city.neg_6));
                        break;
            case -5:   cityList.setAdapter(new ListViewAdapter(this, city.neg_5));
                        break;
            case -4:   cityList.setAdapter(new ListViewAdapter(this, city.neg_4));
                        break;
            case -3:   cityList.setAdapter(new ListViewAdapter(this, city.neg_3));
                        break;
            case -2:   cityList.setAdapter(new ListViewAdapter(this, city.neg_2));
                        break;
            case -1:   cityList.setAdapter(new ListViewAdapter(this, city.neg_1));
                        break;
            case 0:     cityList.setAdapter(new ListViewAdapter(this, city.pos_0));
                        break;
            case 1:     cityList.setAdapter(new ListViewAdapter(this, city.pos_1));
                        break;
            case 2:     cityList.setAdapter(new ListViewAdapter(this, city.pos_2));
                        break;
            case 3:     cityList.setAdapter(new ListViewAdapter(this, city.pos_3));
                        break;
            case 4:     cityList.setAdapter(new ListViewAdapter(this, city.pos_4));
                        break;
            case 5:     cityList.setAdapter(new ListViewAdapter(this, city.pos_5));
                        break;
            case 6:     cityList.setAdapter(new ListViewAdapter(this, city.pos_6));
                        break;
            case 7:     cityList.setAdapter(new ListViewAdapter(this, city.pos_7));
                        break;
            case 8:     cityList.setAdapter(new ListViewAdapter(this, city.pos_8));
                        break;
            case 9:     cityList.setAdapter(new ListViewAdapter(this, city.pos_9));
                        break;
            case 10:    cityList.setAdapter(new ListViewAdapter(this, city.pos_10));
                        break;
            case 11:    cityList.setAdapter(new ListViewAdapter(this, city.pos_11));
                        break;
            case 12:    cityList.setAdapter(new ListViewAdapter(this, city.pos_12));
                        break;
        }
    }


}