package com.crowleysimon.timezonethingy.app;

/**
 * Created by simon on 17/04/2014.
 */
public class Cities {
    public final String[] pos_0 = {
            "test+0",
            "test+0",
            "test+0"
    };

    public final String[] pos_1 =  {
            "test+1",
            "test+1",
            "test+1"
    };

    public final String[] pos_2 =  {
            "test+2",
            "test+2",
            "test+2"
    };
    public final String[] pos_3 =  {
            "test+3",
            "test+3",
            "test+3"
    };
    public final String[] pos_4 =  {
            "test+4",
            "test+4",
            "test+4"
    };
    public final String[] pos_5 =  {
            "test+5",
            "test+5",
            "test+5"
    };
    public final String[] pos_6 =  {
            "test+6",
            "test+6",
            "test+6"
    };
    public final String[] pos_7 =  {
            "test+7",
            "test+7",
            "test+7"
    };
    public final String[] pos_8 =  {
            "test+8",
            "test+8",
            "test+8"
    };
    public final String[] pos_9 =  {
            "test+9",
            "test+9",
            "test+9"
    };
    public final String[] pos_10 =  {
            "test+10",
            "test+10",
            "test+10"
    };
    public final String[] pos_11 =  {
            "test+11",
            "test+11",
            "test+11"
    };
    public final String[] pos_12 =  {
            "test+12",
            "test+12",
            "test+12"
    };
    public final String[] neg_1 =  {
            "test-1",
            "test-1",
            "test-1"
    };
    public final String[] neg_2 =  {
            "test-2",
            "test-2",
            "test-2"
    };
    public final String[] neg_3 =  {
            "test-3",
            "test-3",
            "test-3"
    };
    public final String[] neg_4 =  {
            "test-4",
            "test-4",
            "test-4",
            "test-4",
            "test-4",
            "test-4",
            "test-4",
            "test-4",
            "test-4"
    };
    public final String[] neg_5 =  {
            "Port-au-Prince",
            "Havana",
            "Philadelphia",
            "Miami",
            "New York",

    };
    public final String[] neg_6 =  {
            "Cancun",
            "Austin",
            "Memphis",
            "San Antonio",
            "Minneapolis",
            "Merida",
            "San Jose (CR)",
            "Lincoln",
            "Montgomery",
            "Madison",
            "Jackson",
            "Kansas City",
            "Belmopan",
            "Acapulco",
            "Monterrey",
            "Birmingham (USA)",
            "Oklahoma City",
            "Managua",
            "Bismarck",
            "Houston",
            "New Orleans",
            "Dallas",
            "Galapagos Islands",
            "Nashville",
            "Guatemala",
            "Chicago",
            "Milwaukee",
            "Mexico City",


    };
    public final String[] neg_7 =  {
            "El Paso",
            "Santa Fe",
            "Mesa",
            "Aklavik",
            "Edmonton",
            "Tucson",
            "Boise",
            "Phoenix",
            "Helena",
            "Mazatlan",
            "Albuquerque",
            "Denver",
            "Salt Lake City",
            "Aurora",
            "Calgary",
            "Cheyenne",

    };
    public final String[] neg_8 =  {
            "Portland",
            "Stockton",
            "Surrey",
            "Seattle",
            "Carson City",
            "Long Beach",
            "Whitehorse",
            "Riverside",
            "Anaheim",
            "Salem",
            "Sacramento",
            "Oakland",
            "Los Angeles",
            "San Jose (USA)",
            "San Francisco",
            "San Diego",
            "Las Vegas",
            "Adamstown",
            "Vancouver",
            "Mexicali",
            "Tijuana",
            "Victoria (Canada)",

    };
    public final String[] neg_9 =  {
            "Nome",
            "Anchorage",
            "Fairbanks",
            "Gambier Islands",
            "Juneau",
            "Unalaska",

    };
    public final String[] neg_10 =  {
            "Adak",
            "Papeete",
            "Rarotonga",
            "Honolulu",

    };
    public final String[] neg_11 =  {
            "Pago Pago",
            "Alofi",

    };
    public final String[] neg_12 =  {
            "test-12",
            "test-12",
            "test-12"
    };
}
